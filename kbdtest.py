# -*- coding: utf-8 -*-
"""
Created on Thu Jan 14 13:03:47 2021

@author: mitch
"""


import keyboard


pushed_key = None

def on_keypress (thing):
    """ Callback which runs when the user presses a key.
    """
    global pushed_key

    pushed_key = thing.name




money = 0                # Pursue a career in accounting

keyboard.on_press(on_keypress)  ########## Set callback

# Run a simple loop which responds to some keys and ignores others.
# If someone presses control-C, exit this program cleanly
while True:

    if pushed_key:
        if pushed_key == "0":
            money += 1

        elif pushed_key == '1':
            money += 5

        elif pushed_key == '2':
            money += 10
            
        elif pushed_key == '3':
            money += 25
            
        elif pushed_key == '4':
            money += 100
            
        elif pushed_key == '5':
            money += 500
            
        elif pushed_key == '6':
            money += 1000
            
        elif pushed_key == '7':
            money += 2000
            
       
        elif pushed_key == 'e':
            print ("I've seen an 'E'")
        
        else:
            print('wrong button dummy')
        print(money/100)
        pushed_key = None

