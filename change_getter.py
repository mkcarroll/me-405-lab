'''
@file change_getter.py
@brief Returns your change
@details This file contains a method that will return a tuple containing the
fewest number of denominatins of change that will be issued for a given 
price and payment.
@author Mitchell Carrolll
'''


def getChange(price,payment):
    '''
    @brief A method that returns your change
    '''
    
    vals = (1,5,10,25,100,500,1000,2000)
    
    
    change = payment - price
    
    if change < 0:
        return None
        
    else:
        idx = True
        n=1
        chg = []
        while idx == True:
            test = vals[len(vals)-n]
            if n == len(vals)+1:
                idx = False
            elif change < test:
                chg.insert(0,0)
                n+=1
            elif change >= test:
                num = int(change/test)
                chg.insert(0,num)
                change = change - num*test
                n+=1
        change_den = (chg[0],chg[1],chg[2],chg[3],chg[4],chg[5],chg[6],chg[7])
        return change_den