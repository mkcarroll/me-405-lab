'''
@file mainpage.py
@Brief doc for mainpage.py

Detailed doc for mainpage.py 

@mainpage

@section sec_intro Introduction
This page will serve as an outlet to post documentation for code generated
in MicroPython by Mitchell Carroll. 

@image html hardware.jpg "Harware Used for this Portfolio" width=600px

@section code Source Code
@details Source code can be found at this link:

https://bitbucket.org/mkcarroll/me-405-lab/src/master/

@author Mitchell Carroll
@date Last Updated: January 21, 2021

@page sec_fsm Vendotron Simulation

@section overviewH0 Overview
This project simulates the function of a simple vending machine.

@section fileH0 File List

# vendotron_main.py: A file that organizes the tasks for the vending machine
    
# vendotron_FSM.py: A file that contains the FSM for the vending mahine
    
# kyb_driver.py: A driver for non blocking keyboard input
    
# change_getter.py: A file that returns the change for a payment
    
# shares_lab1.py: A file that contains the shared variables for the vending machine


'''